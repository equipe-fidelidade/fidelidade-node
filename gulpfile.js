var gulp = require('gulp');
var ts = require('gulp-typescript');
var tsProject = ts.createProject('tsconfig.json');
var clean = require('gulp-clean');

gulp.task('watch', ['clean'], () => {
  gulp.watch('src/**/*.*', ['htmlfiles', 'jsonfiles', 'tsfiles']);
});

gulp.task('clean', () => {
    return gulp.src('dist', {force: true})
        .pipe(clean());
});

gulp.task('htmlfiles', () => {
  return gulp.src('src/public/**/*.*')
  .pipe(gulp.dest('dist/public'));
});

gulp.task('jsonfiles', () => {
  return  gulp.src(['src/*.json', 'src/**/*.json', 'src/*.yaml', '*package*.json'])
  .pipe(gulp.dest('dist'));
});

gulp.task('tsfiles', () => {
  var tsResult =  tsProject.src()
  .pipe(tsProject());
  return tsResult.js.pipe(gulp.dest('dist'));
});

gulp.task('default', ['clean', 'htmlfiles', 'jsonfiles',  'tsfiles']);