class Validation {
    constructor() {}
    
    public isOnlyNumbers(numeros: string): boolean{
        let notNumber = /^\d+$/;
        return notNumber.test(numeros);
    }

    public containsNumbers(numeros: string): boolean{
        let notNumber = /^.*[\d].*$/g;
        return notNumber.test(numeros);
    }

    public isCepValid(cep: string): boolean{
        let cepValid = /^\d{5}\-?\d{3}$/;
        return cepValid.test(cep);
    }

    public isDateBrVallid(data: string): boolean{
        let notValid = false;
        if (data && data.length === 10) {
            let dia = data.substring(0, 2);
            let mes = data.substring(3, 5);
            let ano = data.substring(6, 10);
            // Criando um objeto Date usando os valores ano, mes e dia.
            let novaData = new Date(parseInt(ano), (parseInt(mes) - 1), parseInt(dia));
            let mesmoDia = parseInt(dia, 10) == parseInt(novaData.getDate().toString());
            let mesmoMes = parseInt(mes, 10) == parseInt(novaData.getMonth().toString()) + 1;
            let mesmoAno = parseInt(ano) == parseInt(novaData.getFullYear().toString());
            if (!((mesmoDia) && (mesmoMes) && (mesmoAno))) {notValid = true;}
        }else{notValid = true;}
        return !notValid;
    }

    public isHourMninuteValid(hora: string): boolean {
        let notValid = false;
        if (hora && hora.length == 5) {
            let horas = hora.substring(0, 2);
            let minutos = hora.substring(3, 5);
            // Criando um objeto Date usando os valores ano, mes e dia.
            if(horas){
                let horasNumber =  parseInt(horas);
                if(horasNumber > 23){notValid = true;}
            }
            if(minutos){
                let minutosNumber =  parseInt(minutos);
                if(minutosNumber > 59){notValid = true;}
            }
        }else{notValid = true;}
        return !notValid;
    }

    public isCPFValid(CPF) {
        let Resto, Soma = 0;
        CPF = CPF.replace(/\D/g, "")
        if(CPF.length != 11){return false};
        if(["11111111111", "22222222222", "33333333333", "44444444444", "55555555555", 
            "66666666666", "77777777777", "88888888888", "99999999999"].indexOf(CPF) > -1){return false}
        if (CPF == "00000000000") return false;
        for (let i=1; i<=9; i++) Soma = Soma + parseInt(CPF.substring(i-1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;
        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        if (Resto != parseInt(CPF.substring(9, 10)) ) return false;
        Soma = 0;
        for (let i = 1; i <= 10; i++) Soma = Soma + parseInt(CPF.substring(i-1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;
        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        if (Resto != parseInt(CPF.substring(10, 11) ) ) return false;
        return true;
    }

    public isCnpjValid(cnpj: string){
        var notValid = false;
        cnpj = cnpj.replace(/[^0-9]+/g, "");
        if (cnpj.length != 14){
                notValid = true;
        }else if (cnpj == "00000000000000" ||//Elimina CNPJs invalidos conhecidos 
            cnpj == "11111111111111" || 
            cnpj == "22222222222222" || 
            cnpj == "33333333333333" || 
            cnpj == "44444444444444" || 
            cnpj == "55555555555555" || 
            cnpj == "66666666666666" || 
            cnpj == "77777777777777" || 
            cnpj == "88888888888888" || 
            cnpj == "99999999999999"){
                notValid = true; 
            }else{
                let tamanho = cnpj.length - 2;
                let numeros = cnpj.substring(0,tamanho);
                let digitos = cnpj.substring(tamanho);
                let soma = 0;
                let pos = tamanho - 7;
                for (let i = tamanho; i >= 1; i--) {
                     soma += parseInt(numeros.charAt(tamanho - i)) * pos--;
                    if (pos < 2)
                        pos = 9;
                }
                let resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                if (resultado != parseInt(digitos.charAt(0))){
                    notValid = true;
                }else{
                    tamanho = tamanho + 1;
                    numeros = cnpj.substring(0,tamanho);
                    soma = 0;
                    pos = tamanho - 7;
                    for (let i = tamanho; i >= 1; i--) {
                        soma += parseInt(numeros.charAt(tamanho - i)) * pos--;
                        if (pos < 2) pos = 9;
                    }
                    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
                    if (resultado != parseInt(digitos.charAt(1))){
                            notValid = true;				
                    }
                }
            }
        return !notValid;    
    }

    public isEmailValid(email: string): boolean{
        let validador = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return validador.test(email);
    }

    public isEmpty(obj: any){
        if(obj){
            return false;
        }else if(obj instanceof Object){
            for(var key in obj) {
                if(obj.hasOwnProperty(key))  return false;
            }
        }else if(obj instanceof Array && obj.length > 0){
            return false;
        }
        return true;
    }
}
export default new Validation();