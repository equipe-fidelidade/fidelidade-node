export const ConnDB = {
    "CONN_MAIN"                                 :"",
    "LOCAL_MY_SQL"                              : "conn-local-my-sql",
    "DEV_MY_SQL"                                : "conn-mysql-develop",
    "PROD_MY_SQL"                               : "conn-mysql-production",
    "TEST_MY_SQL"                               : "conn-mysql-test"
}

export const secretToken ={
    "TOKEN"                                     : "x-access-token",
    "TIME"                                      : 84600,
    "SECRET"                                    : "fidelidade-node-key"
}
