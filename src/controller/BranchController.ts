import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";
import { DataTypeOrm } from "../database/DataTypeOrm";
import { Branch } from "../entity/Branch";
import { HttpStatus } from "../enum/HtppStatus";
import { MsgBranch, MsgGeneric } from "../enum/MessageTranslate";
import { TypeMSG } from "../enum/TypeMSG";
import { ConnDB } from "../util/Values";

export class BranchController {
    private branchRepository = getRepository(Branch, ConnDB.CONN_MAIN);

    async one(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let idBranch = request.params.id;
        try {
            data.validAddError(!idBranch, MsgGeneric.idNotSet);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                data.obj = await this.branchRepository.findOne({
                    "where":[{"id": idBranch}],
                    "relations": ["branchs"]
                 });
                if(data.obj["id"]){
                    data.send(HttpStatus.OK, response);
                }else{
                    data.hasErrorSend(HttpStatus.NOT_FOUND, response, TypeMSG.DANGER, MsgGeneric.notRecordsFound);
                }
            }
        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorFindRecord, error);
            
        }
    }

    async all(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        try {
           data.joinEntity("company", ["id","fantasy", "cnpj"]);
           await data.findFilter(request, response, this.branchRepository);
           data.send(HttpStatus.OK, response);
        } catch (error) {
            console.log(error);
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorListRecords, error);
        }
    }
    
    async save(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let branchRequest: Branch = request.body;
        try {
            data.validAddError(!branchRequest.fantasy, MsgBranch.fantasyBranchRequired);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                let existBranch = await this.branchRepository.findOne({"fantasy": branchRequest.fantasy});
                data.validAddError(existBranch, MsgBranch.fantasyAlreadyRegistered);
                if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                    data.obj = await this.branchRepository.save(branchRequest);
                    data.send(HttpStatus.CREATED, response, TypeMSG.SUCCESS, MsgGeneric.recordInsertedSuccessfully)
                }
            }
        } catch (error) {
           data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorInsertRecord, error);
        }
    }

    async update(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let idBranch = request.params.id;
        let branchRequest: Branch = request.body;
        try {
            data.validAddError(!idBranch, MsgGeneric.idNotSet);
            data.validAddError(!branchRequest.fantasy, MsgBranch.fantasyBranchRequired);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                let existBranch = await this.branchRepository.findOne(idBranch, {"select": ["id"]});
                data.validAddError(!existBranch, MsgGeneric.notRecordsFound);
                if(!data.hasErrorSend(HttpStatus.NOT_FOUND, response)){
                    let existBranchFantasy = await this.branchRepository.findOne({"fantasy": branchRequest.fantasy});
                    data.validAddError(existBranchFantasy && existBranchFantasy.id != idBranch, MsgBranch.fantasyAlreadyRegistered);
                    if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                        existBranch = await this.branchRepository.merge(existBranch, branchRequest);
                        data.obj = await this.branchRepository.save(existBranch);
                        data.send(HttpStatus.CREATED, response, TypeMSG.SUCCESS, MsgGeneric.recordAlteredSuccessfully)
                    }
                }
            }
        } catch (error) {
           data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorAlterRecord, error);
        }
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let idBranch = request.params.id;
        try {
            data.validAddError(!idBranch, MsgGeneric.idNotSet);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                let BranchToRemove = await this.branchRepository.findOne(idBranch, {"select":["id"]});
                data.validAddError(!BranchToRemove, MsgGeneric.recordNotExist);
                if(!data.hasErrorSend(HttpStatus.NOT_FOUND, response)){
                    data.obj = await this.branchRepository.remove(BranchToRemove);
                    data.send(HttpStatus.OK, response, TypeMSG.SUCCESS, MsgGeneric.recordRemovedSuccessfully);
                }
            }
        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorRemoveRecord, error);
        }
    }
}