import { Request, Response} from "express";
import * as jwt from "jsonwebtoken";
import { getRepository } from "typeorm";
import { DataTypeOrm } from "../database/DataTypeOrm";
import { User } from "../entity/User";
import { HttpStatus } from "../enum/HtppStatus";
import { MsgAuth } from "../enum/MessageTranslate";
import { TypeMSG } from "../enum/TypeMSG";
import UtilService from "../util/UtilService";
import { ConnDB, secretToken } from "../util/Values";
import { NextFunction } from "express-serve-static-core";

export class AuthTokenController{
    private userRepository = getRepository(User, ConnDB.CONN_MAIN);

    public async authenticate(request: Request, response: Response){
        let data: DataTypeOrm = new DataTypeOrm()
        let login: string = request.body.login
        let password: string = request.body.password
        let user: User;
        let profiles: string[];
        let passwordEncript: string;
        try {
            data.validAddError(!login, MsgAuth.loginNotSend);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                passwordEncript = UtilService.encrypt(password);
                user = await this.userRepository.createQueryBuilder("user")
                .leftJoinAndSelect("user.profiles", "profiles")
                .leftJoinAndSelect("profiles.branch", "branch")
                .leftJoinAndSelect("branch.company", "company")
                .leftJoinAndSelect("profiles.actions", "actions")
                .where("user.login = :login AND user.password = :password", {"login": login, "password": passwordEncript})     
                .getOne();
                data.validAddError(!user, MsgAuth.loginPasswordInvalid);
                if(!data.hasErrorSend(HttpStatus.UNAUTHORIZED, response)){
                    user['password'] = '';
                    profiles = data.getUserActions(user);
                    let dataBase = Date.now();
                    let token =  jwt.sign(
                        {   
                            iss                 : 'https://mumps.com.br/', // (Issuer) Origem do token
                            iat                 : Math.floor(dataBase), // (issueAt) Timestamp de quando o token foi gerado
                            exp                 : Math.floor(dataBase + ( 1 * 60 * 60 * 1000 ) ), //(Expiration) Timestamp de quando o token expira
                            sub                 : () => user.id.toString(), //(Subject) Entidade a quem o token pertence
                            login               : user.login,
                            profiles            : profiles
                        }, secretToken.SECRET);
                    data.accessToken = token;
                    // user[secretToken.TOKEN] = token;
                    data.obj = user;
                    data.send(HttpStatus.OK, response, TypeMSG.SUCCESS, MsgAuth.tokenGenerateSuccessfully);
                }
            }
        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgAuth.errorQueryLoginAccess)
        }
    }

    public async validateToken(request: Request, response: Response, next: NextFunction){
        let data: DataTypeOrm = new DataTypeOrm()
        try {
            let token = request.body[secretToken.TOKEN] || request.query[secretToken.TOKEN] || request.headers[secretToken.TOKEN];
            data.validAddError(!token, MsgAuth.notSendToken);
            if(!data.hasErrorSend(HttpStatus.UNAUTHORIZED, response)){
                let userLogged = await jwt.verify(token, secretToken.SECRET);
                data.validAddError(!userLogged, MsgAuth.acessDanied);
                if(!data.hasErrorSend(HttpStatus.UNAUTHORIZED, response)){
                    let profiles: Array<string> = userLogged["profiles"];
                    data.validAddError(!profiles || !profiles.length || profiles.length <= 0, MsgAuth.acessDanied);
                    if(!data.hasErrorSend(HttpStatus.UNAUTHORIZED, response)){
                        let urlOriginal = data.getUrlPattern(4, request.originalUrl);
                        let urlTypeBranch = urlOriginal + "-" + request.method;
                        let branchId = request.body.branchId || request.query.branchId || request.params.branchId || request.headers.branchId;
                        if(branchId){
                            urlTypeBranch += "-" + branchId;
                        }
                        data.validAddError(profiles.indexOf(urlTypeBranch) < 0, MsgAuth.acessDanied);
                        if(!data.hasErrorSend(HttpStatus.UNAUTHORIZED, response)){
                            next();
                        }
                    }
                }
            }
        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgAuth.errorValidToken);
            response.status(500).json(data);
        }
    }

 
}