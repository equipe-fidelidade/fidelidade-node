import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";
import { DataTypeOrm } from "../database/DataTypeOrm";
import { User } from "../entity/User";
import { HttpStatus } from "../enum/HtppStatus";
import { MsgGeneric, MsgUser } from "../enum/MessageTranslate";
import { TypeMSG } from "../enum/TypeMSG";
import Validation from "../util/Validation";
import { ConnDB } from "../util/Values";
import UtilService from "../util/UtilService";

export class UserController {
    private userRepository = getRepository(User, ConnDB.CONN_MAIN);

    async one(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let idUser = request.params.id;
        try {
            data.validAddError(!idUser, MsgGeneric.idNotSet);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                data.obj = await this.userRepository.createQueryBuilder("user")
                    .leftJoinAndSelect("user.profiles", "profiles")
                    .leftJoinAndSelect("profiles.branch", "branch")
                    .leftJoinAndSelect("branch.company", "company")
                    .leftJoinAndSelect("profiles.actions", "actions")
                    .where("user.id = :id", {"id": idUser})     
                    .getOne();
                if(data.obj["id"]){
                    data.send(HttpStatus.OK, response);
                }else{
                    data.hasErrorSend(HttpStatus.NOT_FOUND, response, TypeMSG.DANGER, MsgGeneric.notRecordsFound);
                }
            }

        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorFindRecord, error);
            
        }
    }

    async all(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        try {
           await data.findFilter(request, response, this.userRepository);
           data.send(HttpStatus.OK, response);
        } catch (error) {
            console.log(error);
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorListRecords, error);
        }
    }
    
    
    async save(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let userRequest: User = request.body;
        try {
            data.validAddError(!userRequest.name, MsgUser.nameUserRequired);
            data.validAddError(!userRequest.email, MsgUser.emailUserRequired);
            data.validAddError(!userRequest.cpf, MsgUser.CPFUserRequired);
            data.validAddError(!userRequest.login, MsgUser.loginUserRequired);
            data.validAddError(!userRequest.password, MsgUser.passwordUserRequired);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                userRequest.password = UtilService.encrypt(userRequest.password);
                data.validAddError(!Validation.isCPFValid(userRequest.cpf), MsgGeneric.cpfInvalid);
                data.validAddError(!Validation.isEmailValid(userRequest.email), MsgGeneric.emailInvalid);
                if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                    let existEmail = await this.getUserEmail(userRequest.email, data);
                    data.validAddError(existEmail, MsgGeneric.emailAlreadyRegistered);
                    if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                        let existCPF = await this.getUserCPF(userRequest.cpf, data);
                        data.validAddError(existCPF, MsgGeneric.cpfAlreadyRegistered);
                        if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                            let existLogin = await this.getUserLogin(userRequest.login, data);
                            data.validAddError(existLogin, MsgGeneric.loginAlreadyRegistered);
                            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                                data.obj = await this.userRepository.save(userRequest);
                                data.send(HttpStatus.CREATED, response, TypeMSG.SUCCESS, MsgGeneric.recordInsertedSuccessfully)
                            }
                        }
                    }
                }
            }
        } catch (error) {
           data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorInsertRecord, error);
        }
    }

    async update(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let idUser = request.params.id;
        let userRequest: User = request.body;
        try {
            
            data.validAddError(!idUser, MsgGeneric.idNotSet);
            data.validAddError(!userRequest.name, MsgUser.nameUserRequired);
            data.validAddError(!userRequest.email, MsgUser.emailUserRequired);
            data.validAddError(!userRequest.cpf, MsgUser.CPFUserRequired);
            data.validAddError(!userRequest.login, MsgUser.loginUserRequired);
            // data.validAddError(!userRequest.password, MsgUser.passwordUserRequired);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                userRequest.password = (userRequest.password) ? userRequest.password = UtilService.encrypt(userRequest.password) : undefined;
                let existUser: User = await this.userRepository.findOne(idUser, {"select":["id"]});
                data.validAddError(!existUser, MsgGeneric.notRecordsFound);
                if(!data.hasErrorSend(HttpStatus.NOT_FOUND, response)){
                    data.validAddError(!Validation.isCPFValid(userRequest.cpf), MsgGeneric.cpfInvalid);
                    data.validAddError(!Validation.isEmailValid(userRequest.email), MsgGeneric.emailInvalid);
                    if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                        let existEmail = await this.getUserEmail(userRequest.email, data);
                        data.validAddError(existEmail && existEmail.id != idUser, MsgGeneric.emailAlreadyRegistered);
                        if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                            let existCPF = await this.getUserCPF(userRequest.cpf, data);
                            data.validAddError(existCPF && existCPF.id != idUser, MsgGeneric.cpfAlreadyRegistered);
                            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                                let existLogin = await this.getUserLogin(userRequest.login, data);
                                data.validAddError(existLogin && existLogin.id != idUser, MsgGeneric.cpfAlreadyRegistered);
                                if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                                    existUser = await this.userRepository.merge(existUser, userRequest);
                                    data.obj = await this.userRepository.save(existUser);
                                    data.send(HttpStatus.CREATED, response, TypeMSG.SUCCESS, MsgGeneric.recordAlteredSuccessfully)
                                }
                            }
                        }
                    }
                }
            }
        } catch (error) {
           data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorAlterRecord, error)
        }
    }

    async remove(request: Request, response: Response, next: NextFunction) {
        let data = new DataTypeOrm();
        let idUser = request.params.id;
        try {
            data.validAddError(!idUser, MsgGeneric.idNotSet);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                let userToRemove = await this.userRepository.findOne(request.params.id);
                data.validAddError(!userToRemove, MsgGeneric.recordNotExist);
                if(!data.hasErrorSend(HttpStatus.NOT_FOUND, response)){
                    data.obj = await this.userRepository.remove(userToRemove);
                    data.send(HttpStatus.OK, response, TypeMSG.SUCCESS, MsgGeneric.recordRemovedSuccessfully);
                }
            }
        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorRemoveRecord, error);
        }
    }
    async oneEmail(request: Request, response: Response, next: NextFunction){
       let data = new DataTypeOrm();
       try {
            await this.getUserEmail(request.params.email, data);
            data.validAddError(!data.obj, MsgGeneric.recordNotExist);
            if(!data.hasErrorSend(HttpStatus.NOT_FOUND, response)){
                data.send(HttpStatus.OK, response);
            }
        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorFindRecord, error);
        }
    }

    async oneCPF(request: Request, response: Response, next: NextFunction){
       let data = new DataTypeOrm();
       try {
            await this.getUserCPF(request.params.cpf, data);
            data.validAddError(!data.obj, MsgGeneric.recordNotExist);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                data.send(HttpStatus.OK, response);
            }
        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorFindRecord, error);
        }
    }

    async oneLogin(request: Request, response: Response, next: NextFunction){
       let data = new DataTypeOrm();
       try {
            await this.getUserCPF(request.params.login, data);
            data.validAddError(!data.obj, MsgGeneric.recordNotExist);
            if(!data.hasErrorSend(HttpStatus.BAD_REQUEST, response)){
                data.send(HttpStatus.OK, response);
            }
        } catch (error) {
            data.send(HttpStatus.INTERNAL_SERVER_ERROR, response, TypeMSG.DANGER, MsgGeneric.errorFindRecord, error);
        }
    }

    private async getUserEmail(email: string, data: DataTypeOrm): Promise<User>{
        data.validAddError(!email, MsgUser.emailUserRequired);
        return data.obj = await this.userRepository.findOne({
            "where":[{"email": email}],
            "select": ["id", "name", "photoProfile", "email", "cpf", "login"]
        });
    }

    private async getUserCPF(cpf: string, data: DataTypeOrm): Promise<User>{
        try {
            data.validAddError(!cpf, MsgUser.CPFUserRequired);
           return data.obj = await this.userRepository.findOne({
               "where":[{"cpf": cpf}],
               "select": ["id","name", "photoProfile", "email", "cpf", "login"]
            });
        } catch (error) {
            data.addMSG(TypeMSG.DANGER, MsgGeneric.errorFindRecord);
        }
    }

    private async getUserLogin(login: string, data: DataTypeOrm): Promise<User>{
        try {
            data.validAddError(!login, MsgUser.loginUserRequired);
           return data.obj = await this.userRepository.findOne({
               "where":[{"login": login}],
               "select": ["id","name", "photoProfile", "email", "cpf", "login"]
            });
        } catch (error) {
            data.addMSG(TypeMSG.DANGER, MsgGeneric.errorFindRecord);
        }
    }

}