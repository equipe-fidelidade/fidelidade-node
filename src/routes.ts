import {UserController} from "./controller/UserController";
import { AuthTokenController } from "./controller/AuthTokenController";
import { CompanyController } from "./controller/CompanyController";
import { BranchController } from "./controller/BranchController";
import { ActionController } from "./controller/ActionController";
import { ProfileController } from "./controller/ProfileController";

export const Routes = [
    // ***************** Free
    {method: "get", route: "/v1/user/email/:email", controller: UserController, action: "oneEmail"},
    {method: "get", route: "/v1/user/cpf/:cpf", controller: UserController, action: "oneCPF"},
    {method: "get", route: "/v1/user/login/:login", controller: UserController, action: "oneLogin"},
    // ***************** Autentication e validation
    {method: "post", route: "/v1/authenticate", controller: AuthTokenController, action: "authenticate"},
    {method: "use", route: "/*", controller: AuthTokenController, action: "validateToken"},
    // ***************** User
    {method: "get", route: "/v1/user", controller: UserController, action: "all"},
    {method: "post", route: "/v1/user", controller: UserController, action: "save"},
    {method: "get", route: "/v1/user/:id", controller: UserController, action: "one"}, 
    {method: "put", route: "/v1/user/:id", controller: UserController, action: "update"}, 
    {method: "delete", route: "/v1/user/:id", controller: UserController, action: "remove"},
    // ***************** Companay
    {method: "get", route: "/v1/company", controller: CompanyController, action: "all"},
    {method: "post", route: "/v1/company", controller: CompanyController, action: "save"},
    {method: "get", route: "/v1/company/:id", controller: CompanyController, action: "one"}, 
    {method: "put", route: "/v1/company/:id", controller: CompanyController, action: "update"}, 
    {method: "delete", route: "/v1/company/:id", controller: CompanyController, action: "remove"},
    // ***************** Branch
    {method: "get", route: "/v1/branch", controller: BranchController, action: "all"},
    {method: "post", route: "/v1/branch", controller: BranchController, action: "save"},
    {method: "get", route: "/v1/branch/:id", controller: BranchController, action: "one"}, 
    {method: "put", route: "/v1/branch/:id", controller: BranchController, action: "update"}, 
    {method: "delete", route: "/v1/branch/:id", controller: BranchController, action: "remove"},
    // ***************** Action
    {method: "get", route: "/v1/action", controller: ActionController, action: "all"},
    {method: "post", route: "/v1/action", controller: ActionController, action: "save"},
    {method: "get", route: "/v1/action/:id", controller: ActionController, action: "one"}, 
    {method: "put", route: "/v1/action/:id", controller: ActionController, action: "update"}, 
    {method: "delete", route: "/v1/action/:id", controller: ActionController, action: "remove"},
    // ***************** Profile
    {method: "get", route: "/v1/profile", controller: ProfileController, action: "all"},
    {method: "post", route: "/v1/profile", controller: ProfileController, action: "save"},
    {method: "get", route: "/v1/profile/:id", controller: ProfileController, action: "one"}, 
    {method: "put", route: "/v1/profile/:id", controller: ProfileController, action: "update"}, 
    {method: "delete", route: "/v1/profile/:id", controller: ProfileController, action: "remove"},
];