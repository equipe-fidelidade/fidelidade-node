import { Connection, createConnection } from "typeorm";
import { ConnDB } from "../util/Values";
export class FactoryDB {
    constructor(){}
    private mainConnectionMySql: Connection;
    public async createConnectionMySql(connection: string): Promise<Connection>{
        try {
            console.log("--------------- Init Connection DataBase");
            this.mainConnectionMySql = await createConnection(connection);
            console.log(`Connection successuly: ${connection}!`);
            return this.mainConnectionMySql;
        } catch (error) {
            console.log(`Error on try connect in: ${ConnDB.CONN_MAIN} \n ${error}`);
        }
    }
    
    public async closeConnectionPostgres(onClose: Function, onError: Function){
        console.log("Try Close Connect");
        try {
            await this.mainConnectionMySql.close();
            onClose();
        } catch (error) {
            onError();
        }

    }
}