import { Request, Response } from "express";
import { Like, Not, Repository } from "typeorm";
import { TypeMSG } from "../enum/TypeMSG";
import UtilService from "../util/UtilService";
import Validation from "../util/Validation";
import { HttpStatus } from "../enum/HtppStatus";

export class DataTypeOrm {
    //Objetos padrões
    public obj: Object;
    public list: Array<any>;
    public messages: Array<any>;
    public accessToken: string;

    public page: number;
    public totalPages: number;
    public limit: number;
    public rangeInit: number;
    public rangeEnd: number;
    public field: string;
    public qtdTotal: number;
    public offset: number;
    public filter: any;
    private ands: Array<any>;
    private nots: Array<any>;
    private joins: Array<String>;
    private fieldsJoins: Array<String>;
    /**
     * Função que retorna a lista de mensagem criadas, caso esteja nula ela será instanciada.
     */
    private getMessages(): Array<any> {
        if (!this.messages) {
            this.messages = new Array();
        }
        return this.messages;
    }

    private getAnds(): Array<any> {
        if (!this.ands) {
            this.ands = new Array();
        }
        return this.ands;
    }
    /**
     * Função utilizada para adicionar uma mensagem a lista de mensagem que será retornada no response.
     * @param typeMsg O tipo de mensagem que será exibida pelo Client.
     * @param textMsg O texto que será apresentado pelo Client(normalmente envia-se um código que será substituido no Client)
     */
    public addMSG(typeMsg: TypeMSG, textMsg: string) {
        if (typeMsg && textMsg) {
            this.getMessages().push({ type: typeMsg, msg: textMsg });
        }
    }

    /**
     * Função que verifica se existem alguma mensagem do tipo DANGER na lista de mensagem.
     */
    public hasErrors(): boolean {
        if (this.messages && this.messages.length > 0) {
            return this.messages.some((msg) => msg.type && msg.type == TypeMSG.DANGER);
        }
        return false;
    }
    /**
     * Verifica se a condição é verdadeira, caso não seja será adicionada uma mensagem com a mensagem passado por parâmetro.
     * @param conditionValidation 
     * @param msgError 
     */
    public validAddError(conditionValidation: any, msgError: string) {
        if (conditionValidation) {
            this.addMSG(TypeMSG.DANGER, msgError);
        }
    }

    private getFilter(): any {
        if (!this.filter) {
            this.filter = {};
        }
        return this.filter;
    }

    /**
     * Função que faz a preparação e criação dos atributos para o filtro e caso nescessário a paginação
     *  e também faz execução da query.
     * @param request Objeto de requisição.
     * @param response Objeto de resposta
     * @param model O model que será responsável de identifcar qual objeto será utilizado na query.
     */
    public async findFilter(request: Request, response: Response, repository: Repository<any>) {
        try {
            //Palavras que nâo entrarão no where.
            let wordKeys: Array<string> = ["page", "limit", "asc", "desc", "range", "fields", "notin"];

            let LIMIT_PAGE = 10;

            let asc = request.query.asc && request.query.asc.indexOf(",") > -1 ? request.query.asc.split(',') : request.query.asc;
            let desc = request.query.desc && request.query.desc.indexOf(",") > -1 ? request.query.desc.split(',') : request.query.desc;
            let fields = request.query.fields && request.query.fields.indexOf(",") > -1 ? request.query.fields.split(',') : request.query.fields;
            this.page = request.query.page ? request.query.page : undefined;
            this.limit = (request.query.limit || this.page) ? request.query.limit || LIMIT_PAGE : undefined;
            if (this.page && this.limit) {
                this.offset = (this.page - 1) * this.limit;
            }
            let orderParams = UtilService.getCriteriosOrdenacao(asc, desc, "id");
            //Percorrendo todos os parâmetros e adicionando no where.
            for (let key in request.query) {
                if (request.query[key] && wordKeys.indexOf(key) < 0) {
                    this.addAND(key, request.query[key]);
                }
            }
            //Verificando se foi passado o parâmetro fields e limitando os campos que serão retornados.
            if (fields) {
                fields = [].concat(fields);
                this.getFilter()["select"] = fields;
            }
            if (orderParams) {
                this.getFilter()["order"] = orderParams;
            }
            if (this.limit) {
                this.getFilter()["take"] = typeof this.limit == "string" ? parseInt(this.limit) : this.limit;
            }
            if (this.limit) {
                this.getFilter()["skip"] = this.offset;
            }
            if (this.joins && this.joins.length > 0) {
                if (this.fieldsJoins && this.fieldsJoins.length > 0) {
                    this.joins.concat(this.fieldsJoins);
                }
                this.getFilter()["relations"] = this.joins;
            }//Se caso exista os ands
            let where = [];
            if (this.ands && this.ands.length > 0) {
                this.ands.forEach(and => {
                    where.push(and);
                });
            }
            // where.push({[this.Op.between] : [{id: [1,3]}]});
            if (where.length > 0) {
                this.getFilter()["where"] = where;
            }
            console.log(this.getFilter());
            let [list, count] = await repository.findAndCount(this.getFilter());

            this.qtdTotal = count;
            if (this.page && this.limit) {//Verificando se está usando paginação e criando os rangeInit e rangeEnd
                this.calcRanges();
            }
            this.list = list;
            //Limpando dados que não precisam está no objeto..
            this.ands = undefined; this.filter = undefined; this.fieldsJoins = undefined;
            this.joins = undefined, this.nots = undefined;
            return this;
        } catch (error) {
            console.log(error);
        }
    }
    // /**
    //  * Função que calcula o intervalo de registro retornado pela query executada.
    //  */
    private calcRanges() {
        if (this.page > 0 && this.qtdTotal > 0) {
            this.totalPages = Math.ceil(this.qtdTotal / this.limit);
        }
        if (this.page < this.totalPages) {
            this.rangeEnd = this.page * this.limit;
        } else {
            this.rangeEnd = this.qtdTotal;
            this.page = this.totalPages;
        }
        if (this.page != this.totalPages) {
            this.rangeInit = (this.rangeEnd + 1) - this.limit
        } else {
            this.rangeInit = ((this.totalPages - 1) * this.limit) + 1;
        }
    }
    // /**
    //  * Função que verifica se o campo é do tipo número, data ou se não contem o caracter "%",
    //  *  caso ele seja do tipo string e tiver o "%" será adicionada um like.
    //  * @param field Nome do campo será criado para pesquisa.
    //  * @param value Valor do campo será criado para pesquisa.
    //  */
    private setFieldSearch(field: string, value: any, isNot?: boolean) {
        let obj = {};
        if (Validation.isOnlyNumbers(value) || Validation.isDateBrVallid(value) || value.indexOf("%") < 0) {
            if (isNot) {
                obj[field] = Not(value);
            } else {
                obj[field] = value;
            }
        } else {
            if (isNot) {
                obj[field] = Not(Like(value));
            } else {
                obj[field] = Like(value);
            }
        }
        return obj;
    }

    /**
     * Função que faz um Join do objeto que será executada a query com outro objeto.
     * @param entity Modelo que será realizado o join.
     * @param fields Os campos referente ao Modelo do join que serão retornados.
     */
    public joinEntity(entity: string, fields?: String | string[]) {
        if (!this.joins) {
            this.joins = [];
        }
        this.joins.push(entity);
        if (fields) {
            if (!this.fieldsJoins) {
                this.fieldsJoins = [];
            }
            if (Array.isArray(fields)) {
                fields.forEach((field) => {
                    this.fieldsJoins.push(`${entity}.${field}`);
                })
            } else {
                this.fieldsJoins.push(`${entity}.${fields}`);
            }
        }
    }
    // /**
    //  * Criar condição AND no Objeto que será executada a query.
    //  * @param field Nome do campo.
    //  * @param value Valor do campo.
    //  */
    public addAND(field: string, value: any) {
        if (value) {
            this.getAnds().push(
                this.setFieldSearch(field, value)
            );
        }
    }

    /**
     * Criar condição NOT AND no Objeto que será executada a query.
     * @param field Nome do campo.
     * @param value Valor do campo.
     */
    public addNotIn(field: string, value: Array<any>) {
        if (value) {
            this.getAnds().push(
                this.setFieldSearch(field, value, true)
            );
        }
    }

    /**
     * Retorna a url no formado original do express sem os parâmetros.
     * @param versao A primeira parte utilizada na url.
     * @param urlOriginal A url direta do request com todos os parâmetros.
     */
    public getUrlPattern(versionLength: number, urlOriginal: string) {
        let urlOutParams = (urlOriginal.indexOf("?") > -1)
            ? urlOriginal.substring(0, urlOriginal.indexOf("?"))
            : urlOriginal.substring(0);
        let version = (versionLength > 0)
            ? urlOutParams.substring(0, versionLength)
            : "";
        let urlOutVersion = urlOutParams.substring(versionLength);
        let dominio = (urlOutVersion.indexOf("/") > -1)
            ? urlOutVersion.substring(0, urlOutVersion.indexOf("/"))
            : urlOutVersion;
        let parametros = urlOutVersion.substring(dominio.length + 1);
        let partes = parametros.split("/");
        parametros = "";
        if (partes && partes.length > 0) {
            let value = partes[0];
            if (value && partes.length == 1) {
                if (Validation.isOnlyNumbers(value)) {
                    parametros += "/:id";
                } else {
                    parametros += "/" + value;
                }
            } else {
                partes.forEach((p, index) => {
                    if (index > 0 && (index + 1) % 2 == 0) {
                        let campo = partes[index - 1];
                        parametros += "/" + campo + "/:" + campo;
                    }
                });
            }
        }
        return version + dominio + parametros;
    }
    /**
     * 
     * @param user Usuário Logado.
     * @returns Retorna as ações do usuário.
     */
    public getUserActions(user: any): Array<string> {
        let userActions: Array<string> = [];
        let profiles = user["profiles"];
        if (profiles && profiles.length) {
            profiles.forEach(profile => {
                let actions = profile["actions"];
                let branchId = profile["branch"] ? profile["branch"]["id"] : undefined;
                if (actions) {
                    actions.forEach(action => {
                        let urlTypeBranch = action.url + "-" + action.method;
                        if (branchId) { urlTypeBranch += "-" + branchId; }
                        if (urlTypeBranch && userActions.indexOf(urlTypeBranch) < 0) {
                            userActions.push(urlTypeBranch);
                        }
                    });
                }
            });
        }
        return userActions;
    }

    public send(status: number, response: Response, typeMsg?: TypeMSG, textMsg?: string, error?: any): DataTypeOrm {
        if (error) {
            console.log(`LOG - STATUS: ${status} - MSG: ${textMsg} - Error: ${error}`);
        }
        this.addMSG(typeMsg, textMsg);
        response.status(status).json(this);
        return this;
    }

    public hasErrorSend(status: number, response: Response, typeMsg?: TypeMSG, textMsg?: string): boolean {
        this.addMSG(typeMsg, textMsg);
        if (this.hasErrors()) {
            this.obj = undefined;
            this.list = undefined;
            this.send(status, response);
            return true;
        }
        return false;
    }
}