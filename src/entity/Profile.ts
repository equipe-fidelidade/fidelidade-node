import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, ManyToMany, JoinTable} from "typeorm";
import { Branch } from "./Branch";
import { User } from "./User";
import { Action } from "./Action";

@Entity()
export class Profile {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false, length: 100})
    name: string;

    @ManyToOne(type => Branch, branch => branch.profiles)
    branch: Branch;

    @ManyToMany(type => User, user => user.profiles)
    users: User[];

    @ManyToMany(() => Action, (action: Action) => action.profiles, {cascade: true})
    @JoinTable()
    actions: Action[];
}
