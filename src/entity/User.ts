import { Column, Entity, PrimaryGeneratedColumn, ManyToMany, JoinTable } from "typeorm";
import { Profile } from "./Profile";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: false, length: 100})
    name: string;
    
    @Column({nullable: false, length: 11})
    cpf: string;

    @Column({nullable: false, length: 100})
    email: string;

    @Column({nullable: false, length: 100})
    login: string;
    
    @Column({nullable: false, length: 100})
    password: string;
    
    @Column({name: "photo_profile", nullable: true, length: 200})
    photoProfile: string;

    @Column({nullable: false, name: "is_admin", default: false,})
    isAdmin: boolean;

    @Column({nullable: false, name: "is_active", default: true,})
    isActive: boolean;

    @ManyToMany(type => Profile, profile => profile.users, {nullable: true, cascade: true})
    @JoinTable()
    profiles: Profile[];


}
