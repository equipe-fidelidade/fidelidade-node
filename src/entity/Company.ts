import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import { Branch } from "./Branch";
import { Status } from "../enum/Status";

@Entity()
export class Company {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: true, length: 100, unique: true})
    cnpj: string;

    @Column({nullable: false, length: 100})
    fantasy: string;
    
    @Column({
        nullable: false,
        type: "enum",
        enum: Status,
        default: Status.active,
    })
    status: Status;

    @OneToMany(type => Branch, branch => branch.company)
    branchs: Branch[];
}
