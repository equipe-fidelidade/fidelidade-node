import {Entity, PrimaryGeneratedColumn, Column, JoinTable, ManyToMany} from "typeorm";
import { Profile } from "./Profile";
import { Method } from "../enum/Method";

@Entity()
export class Action {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: true, name: "menu_label", length: 60, comment : 'Label que será mostrado no menu.', default: null})
    menuLabel: string;

    @Column({nullable: true, length: 30, comment : 'Rota da ação.'})
    router: string;

    @Column({nullable: false, length: 100, comment : 'Url da ação.'})
    url: string;

    @Column({nullable: false, length: 30, comment : 'Acão executada da ação.'})
    name: string;
    
    @Column({nullable: true, length: 30, comment : 'O grupo que será mostrado na menu'})
    group: string;
    
    @Column({nullable: true, length: 30, comment : 'Icon da ação.'})
    icon: string;

    @Column({nullable: false, name: "is_admin", default: false})
    isAdmin: boolean;
   
    @Column({
        nullable: false,
        type: "enum",
        enum: Method,
    })
    method: Method;

    @ManyToMany(() => Profile, (profile: Profile) => profile.actions)
    profiles: Profile[];


}
