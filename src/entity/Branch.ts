import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, OneToMany} from "typeorm";
import { Company } from "./Company";
import { Profile } from "./Profile";
import { Status } from "../enum/Status";

@Entity()
export class Branch {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({nullable: true, length: 100, unique: true})
    cnpj: string;

    @Column({nullable: true, length: 100})
    fantasy: string;
    
    @Column({
        nullable: false,
        type: "enum",
        enum: Status,
        default: Status.active,
    })
    status: Status;
    
    @Column({name: "color_one", nullable: true, length: 20, comment: "Cor primária que será usado no layout do client"})
    colorOne: string;
    
    @Column({name: "color_two", nullable: true, length: 20, comment: "Cor secundária que será usado no layout do client"})
    colorTwo: string;
    
    @Column({name: "color_three", nullable: true, length: 20, comment: "Cor terciária que será usado no layout do client"})
    colorThree: string;

    @ManyToOne(type => Company, company => company.branchs)
    company: Company;

    @OneToMany(type => Profile, profile => profile.branch)
    profiles: Profile[];

}
